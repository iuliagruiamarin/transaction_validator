package com.test.tranzationvalidator.service;

import com.test.tranzationvalidator.dto.FinancialTransactionDTO;
import com.test.tranzationvalidator.dto.ValidationMessageDTO;
import com.test.tranzationvalidator.enums.ValidationStatus;

public interface TransactionService {


    /*
     * Validate financial transaction
     * @param financialTransaction the transaction that will be validated
     * @return the validation message
     */
    ValidationMessageDTO validateFinancialTransaction(FinancialTransactionDTO financialTransaction);

}
