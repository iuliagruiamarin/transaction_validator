package com.test.tranzationvalidator.service;

import com.test.tranzationvalidator.dto.FinancialTransactionDTO;
import com.test.tranzationvalidator.dto.ValidationMessageDTO;
import com.test.tranzationvalidator.enums.TransactionType;
import com.test.tranzationvalidator.enums.ValidationStatus;
import com.test.tranzationvalidator.utils.TransactionUtils;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class TransactionServiceImpl implements TransactionService {


    @Override
    public ValidationMessageDTO validateFinancialTransaction(FinancialTransactionDTO financialTransaction) {
        if (financialTransaction.getPayeeName() == null || financialTransaction.getPayerName() == null ||
                financialTransaction.getAmount() == null || financialTransaction.getTransactionType() == null) {
            return new ValidationMessageDTO(ValidationStatus.INVALID, "Some mandatory information are missing");
        }
        if (!TransactionUtils.isCnpValid(financialTransaction.getCnpPayee()) || !TransactionUtils.isCnpValid(financialTransaction.getCnpPayer())) {
            return new ValidationMessageDTO(ValidationStatus.INVALID, "CNPs are invalid. Please recheck them.");
        }
        if (financialTransaction.getTransactionType().equals(TransactionType.IBAN_TO_IBAN)) {
            return checkIbanToIbanTransaction(financialTransaction);
        }
        if (financialTransaction.getTransactionType().equals(TransactionType.IBAN_TO_WALLET)) {
            return checkIbanToWallet(financialTransaction);
        }
        if (financialTransaction.getTransactionType().equals(TransactionType.WALLET_TO_IBAN)) {
            return checkWalletToIban(financialTransaction);
        }
        if (financialTransaction.getTransactionType().equals(TransactionType.WALLET_TO_WALLET)) {
            return checkWalletToWallet(financialTransaction);
        }
        return new ValidationMessageDTO(ValidationStatus.INVALID, "Unknown transaction type");
    }

    private ValidationMessageDTO checkWalletToWallet(FinancialTransactionDTO financialTransaction) {
        if (financialTransaction.getWalletPayer() == null || financialTransaction.getWalletPayee() == null) {
            return new ValidationMessageDTO(ValidationStatus.INVALID, "Wallets id can not be NULL");
        }
        return new ValidationMessageDTO(ValidationStatus.VALID, "All the information are valid");
    }

    private ValidationMessageDTO checkWalletToIban(FinancialTransactionDTO financialTransaction) {
        if (financialTransaction.getIbanPayee() == null || financialTransaction.getWalletPayer() == null) {
            return new ValidationMessageDTO(ValidationStatus.INVALID, "IBAN Payee and Wallet Payer id can not be NULL");
        }
        if (!TransactionUtils.isIbanValid(financialTransaction.getIbanPayee())) {
            return new ValidationMessageDTO(ValidationStatus.INVALID, "Payee IBAN is invalid. Please recheck it.");
        }
        return new ValidationMessageDTO(ValidationStatus.VALID, "All the information are valid");
    }

    private ValidationMessageDTO checkIbanToWallet(FinancialTransactionDTO financialTransaction) {
        if (financialTransaction.getIbanPayer() == null || financialTransaction.getWalletPayee() == null) {
            return new ValidationMessageDTO(ValidationStatus.INVALID, "IBAN Payer and Wallet Payee id can not be NULL");
        }
        if (!TransactionUtils.isIbanValid(financialTransaction.getIbanPayer())) {
            return new ValidationMessageDTO(ValidationStatus.INVALID, "Payer IBAN is invalid. Please recheck it.");
        }
        return new ValidationMessageDTO(ValidationStatus.VALID, "All the information are valid");
    }

    private ValidationMessageDTO checkIbanToIbanTransaction(FinancialTransactionDTO financialTransaction) {
        if (financialTransaction.getIbanPayer() == null || financialTransaction.getIbanPayee() == null) {
            return new ValidationMessageDTO(ValidationStatus.INVALID, "IBANs can not be NULL");
        }
        if (!TransactionUtils.isIbanValid(financialTransaction.getIbanPayee()) || !TransactionUtils.isIbanValid(financialTransaction.getIbanPayer())) {
            return new ValidationMessageDTO(ValidationStatus.INVALID, "IBANs are invalid. Please recheck them.");
        }
        return new ValidationMessageDTO(ValidationStatus.VALID, "All the information are valid");
    }
}
