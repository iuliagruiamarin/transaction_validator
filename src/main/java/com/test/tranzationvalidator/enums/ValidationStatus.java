package com.test.tranzationvalidator.enums;

public enum ValidationStatus {
    VALID,
    INVALID;
}
