package com.test.tranzationvalidator.dto;

import com.test.tranzationvalidator.enums.ValidationStatus;

public class ValidationMessageDTO {

    private ValidationStatus validationStatus;

    private String validationDetails;

    public ValidationMessageDTO(ValidationStatus validationStatus, String validationDetails) {
        this.validationStatus = validationStatus;
        this.validationDetails = validationDetails;
    }

    public ValidationStatus getValidationStatus() {
        return validationStatus;
    }

    public void setValidationStatus(ValidationStatus validationStatus) {
        this.validationStatus = validationStatus;
    }

    public String getValidationDetails() {
        return validationDetails;
    }

    public void setValidationDetails(String validationDetails) {
        this.validationDetails = validationDetails;
    }
}
