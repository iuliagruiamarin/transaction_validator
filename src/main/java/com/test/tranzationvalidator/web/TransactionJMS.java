package com.test.tranzationvalidator.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.tranzationvalidator.dto.FinancialTransactionDTO;
import com.test.tranzationvalidator.dto.ValidationMessageDTO;
import com.test.tranzationvalidator.service.TransactionService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import java.io.IOException;

@Component
public class TransactionJMS {


    private Logger logger = LoggerFactory.getLogger(TransactionJMS.class);

    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    TransactionService transactionService;

    private ModelMapper modelMapper = new ModelMapper();


    @Autowired
    private JmsTemplate jmsTemplate;

    @JmsListener(destination = "TRANSACTION.QUEUE")
    public void receiveMessage(String financialTransaction, Message message) throws IOException {
        logger.info(" >> Original received message: " + message);
        logger.info(" >> Received financial transaction: " + financialTransaction);
        try {
            Thread.sleep(4000);
            logger.info("Sleeping on demo purpose...");
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw  new IllegalStateException("Random Interrupted thread");
        }
        ValidationMessageDTO validationMessageDTO = transactionService.validateFinancialTransaction(mapper.readValue(financialTransaction, FinancialTransactionDTO.class));
        jmsTemplate.send("VALIDATION.QUEUE", new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                Message validationMessage = null;
                try {
                    logger.info("Preparing validation response...");
                    validationMessage = session.createTextMessage(mapper.writeValueAsString(validationMessageDTO));
                    validationMessage.setJMSCorrelationID(message.getStringProperty("ftid"));
                    validationMessage.setJMSDestination(message.getJMSReplyTo());
                    logger.info("Validation message prepared...");
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
                logger.info("Validation message: " + validationMessage.toString());
                return validationMessage;
            }
        });
        logger.info("Validation message sent...");
    }
}
