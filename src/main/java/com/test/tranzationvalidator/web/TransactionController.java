package com.test.tranzationvalidator.web;

import com.test.tranzationvalidator.dto.FinancialTransactionDTO;
import com.test.tranzationvalidator.dto.ValidationMessageDTO;
import com.test.tranzationvalidator.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/transaction")
public class TransactionController {

    @Autowired
    private TransactionService transactionService;


    @PostMapping(path = "/validate", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity<ValidationMessageDTO> validateFinancialTransaction(@RequestBody FinancialTransactionDTO financialTransactionDTO) {
        return ResponseEntity.ok(transactionService.validateFinancialTransaction(financialTransactionDTO));
    }
}
