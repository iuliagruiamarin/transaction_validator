package com.test.tranzationvalidator.utils;

import org.apache.commons.validator.routines.IBANValidator;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class TransactionUtils {
    public static List<Integer> controlNumberList = Arrays.stream("279146358279".split("")).map(Integer::valueOf).collect(Collectors.toList());


    public static boolean isIbanValid(String iban) {
        return IBANValidator.DEFAULT_IBAN_VALIDATOR.isValid(iban);
    }

    public static boolean isCnpValid(String cnp) {
        if (cnp == null) {
            return false;
        }
        if (cnp.length() != 13) {
            return false;
        }
        List<Integer> numberList = Arrays.stream(cnp.split("")).map(Integer::valueOf).collect(Collectors.toList());
        int sum = 0;
        for (int i = 0; i < controlNumberList.size(); i++) {
            sum = sum + numberList.get(i) * controlNumberList.get(i);

        }
        int controlNumber;
        if (sum % 11 == 10) {
            controlNumber = 1;
        } else controlNumber = sum % 11;

        return controlNumber == numberList.get(12);
    }
}
